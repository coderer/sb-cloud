package com.lxl.microservicesimleconsumermovie.config;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @desc: feign日志配置类
 * @date & @author: 2018/11/23 11:31 & lxl
 */
@Configuration
public class FeignLogConfiguration {

    /**
     * feign 日志等级
     * @return Logger.level
     */
    @Bean
    public Logger.Level feignLoggerLevel() {
        return Logger.Level.FULL;
    }
}
