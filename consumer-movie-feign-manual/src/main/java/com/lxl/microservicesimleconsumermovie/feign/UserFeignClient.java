package com.lxl.microservicesimleconsumermovie.feign;

import com.lxl.microservicesimleconsumermovie.entity.User;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @desc: 请求用户服务的feign接口
 * @date & @author: 2018/11/20 17:31 & lxl
 */
public interface UserFeignClient {

    /**
     * 根据id查找用户
     *
     * @param id 用户id
     * @return User
     */
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    User findById(@PathVariable("id") Long id);

}
