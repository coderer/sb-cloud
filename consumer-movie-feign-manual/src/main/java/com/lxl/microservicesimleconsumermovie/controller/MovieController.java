package com.lxl.microservicesimleconsumermovie.controller;

import com.lxl.microservicesimleconsumermovie.config.FeignLogConfiguration;
import com.lxl.microservicesimleconsumermovie.entity.User;
import com.lxl.microservicesimleconsumermovie.feign.UserFeignClient;
import feign.Client;
import feign.Contract;
import feign.Feign;
import feign.auth.BasicAuthRequestInterceptor;
import feign.codec.Decoder;
import feign.codec.Encoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.openfeign.FeignClientsConfiguration;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


/**
 * @desc: 用户controller
 * @author: lxl
 * @date: 2018/11/9 16:01
 * @version: v1.0
 */
@RestController
@Import({FeignClientsConfiguration.class, FeignLogConfiguration.class})
public class MovieController {

    private UserFeignClient userUserFeignClient;

    private UserFeignClient adminUserFeignClient;

    @Autowired
    public MovieController(Decoder decoder, Encoder encoder, Client client, Contract contract) {
        this.userUserFeignClient = Feign.builder()
                .client(client)
                .encoder(encoder)
                .decoder(decoder)
                .contract(new SpringMvcContract())
                .requestInterceptor(new BasicAuthRequestInterceptor("user", "password1"))
                .target(UserFeignClient.class, "http://provider-user-with-auth/");

        this.adminUserFeignClient = Feign.builder()
                .client(client)
                .encoder(encoder)
                .decoder(decoder)
                .contract(new SpringMvcContract())
                .requestInterceptor(new BasicAuthRequestInterceptor("admin", "password2"))
                .target(UserFeignClient.class, "http://provider-user-with-auth/");
    }

    /**
     * 根据id查找用户
     *
     * @param id 用户id
     * @return User
     */
    @GetMapping("/user-user/{id}")
    public User findById(@PathVariable Long id) {
        return this.userUserFeignClient.findById(id);
    }

    /**
     * 根据id查找用户
     *
     * @param id 用户id
     * @return User
     */
    @GetMapping("/user-admin/{id}")
    public User findUserById(@PathVariable Long id) {
        return this.adminUserFeignClient.findById(id);
    }
}
