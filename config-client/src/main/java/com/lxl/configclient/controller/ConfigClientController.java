package com.lxl.configclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 获得配置 controller
 */
@RestController
@RefreshScope
public class ConfigClientController {

    @Value("${profile}")
    private String profile;

    /**
     * 从gitee上获得配置
     *
     * @return String
     */
    @GetMapping("/profile")
    public String getProperties() {
        return this.profile;
    }
}
