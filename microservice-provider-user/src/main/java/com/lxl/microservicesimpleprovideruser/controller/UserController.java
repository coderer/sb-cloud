package com.lxl.microservicesimpleprovideruser.controller;

import com.lxl.microservicesimpleprovideruser.dao.UserRepository;
import com.lxl.microservicesimpleprovideruser.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * @desc: 用户controller
 * @author: lxl
 * @date: 2018/11/9 16:01
 * @version: v1.0
 */
@RestController
public class UserController {

    /** 用户Repository */
    private final UserRepository userRepository;

    @Autowired
    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    /**
     * 根据id获取用户信息
     * @param id 用户id
     * @return User
     */
    @GetMapping("/{id}")
    public User findById(@PathVariable Long id) {
        Optional<User> user = userRepository.findById(id);
        return user.orElse(null);
    }

    @GetMapping("/{id}/{name}")
    public User findByIdAndName(@PathVariable("id") Long id, @PathVariable("name") String name) {
        return this.userRepository.findByIdAndName(id, name);
    }

    @PostMapping("/{id}/{name}")
    public User getByIdAndName(@PathVariable("id") Long id, @PathVariable("name") String name) {
        return this.userRepository.findByIdAndName(id, name);
    }


    @GetMapping("/user")
    public User getUserByIdAndNameGet(@RequestParam("id") Long id, @RequestParam("name") String name) {
        return this.userRepository.findByIdAndName(id, name);
    }

    @PostMapping("/user")
    public User getUserByIdAndNamePost(@RequestParam("id") Long id, @RequestParam("name") String name) {
        return this.userRepository.findByIdAndName(id, name);
    }

    @PostMapping("/user/entity")
    public User getUserPost(@RequestBody User user) {
        return this.userRepository.findByIdAndName(user.getId(), user.getName());
    }

}
