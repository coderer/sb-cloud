package com.lxl.microservicesimpleprovideruser.controller;

import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

/**
 * @desc: 文件上传 controller
 * @date & @author: 2018/11/28 14:26 & lxl
 */
@Controller
public class FileUploadController {

    /**
     * 文件上传
     * @param file 文件
     * @return 文件路径
     * @throws IOException io异常
     */
    @PostMapping("upload")
    public @ResponseBody
    String handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
        byte[] bytes = file.getBytes();
        File fileToSave = new File(Objects.requireNonNull(file.getOriginalFilename()));
        FileCopyUtils.copy(bytes, fileToSave);
        String absolutePath = fileToSave.getAbsolutePath();
        System.out.println("文件保存路径: " + absolutePath);
        return absolutePath;
    }
}
