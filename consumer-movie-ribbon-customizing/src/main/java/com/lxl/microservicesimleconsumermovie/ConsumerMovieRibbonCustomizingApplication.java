package com.lxl.microservicesimleconsumermovie;

import com.lxl.microservicesimleconsumermovie.ribbon.RibbonConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = RibbonConfiguration.class))
public class ConsumerMovieRibbonCustomizingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerMovieRibbonCustomizingApplication.class, args);
    }

}
