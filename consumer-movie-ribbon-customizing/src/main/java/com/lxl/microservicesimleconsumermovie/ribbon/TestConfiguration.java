// package com.lxl.microservicesimleconsumermovie.ribbon;
//
// import org.springframework.cloud.netflix.ribbon.RibbonClient;
// import org.springframework.context.annotation.Configuration;
//
// /**
//  * @desc: 测试ribbon规则
//  * @date & @author: 2018/11/20 16:17 & lxl
//  */
// @Configuration
// @RibbonClient(name = "microservice-provider-user-my-metadata", configuration = RibbonConfiguration.class)
// public class TestConfiguration {
//
// }
