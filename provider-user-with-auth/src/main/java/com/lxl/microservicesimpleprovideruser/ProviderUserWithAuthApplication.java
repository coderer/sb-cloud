package com.lxl.microservicesimpleprovideruser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @desc: 启动类
 * @author: lxl
 * @date: 2018/11/9 16:08
 * @version: v1.0
 */
@SpringBootApplication
@EnableDiscoveryClient
public class ProviderUserWithAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProviderUserWithAuthApplication.class, args);
    }
}
