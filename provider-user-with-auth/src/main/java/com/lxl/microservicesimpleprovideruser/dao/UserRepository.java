package com.lxl.microservicesimpleprovideruser.dao;

import com.lxl.microservicesimpleprovideruser.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @desc: 用户dao
 * @author: lxl
 * @date: 2018/11/9 16:00
 * @version: v1.0
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
