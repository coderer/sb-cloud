package com.lxl.microservicesimpleprovideruser.controller;

import com.lxl.microservicesimpleprovideruser.dao.UserRepository;
import com.lxl.microservicesimpleprovideruser.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;

/**
 * @desc: 用户controller
 * @author: lxl
 * @date: 2018/11/9 16:01
 * @version: v1.0
 */
@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/{id}")
    public User findById(@PathVariable("id") Long id) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof UserDetails) {
            UserDetails userDetails = (UserDetails) principal;

            Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();

            for (GrantedAuthority authority : authorities) {
                logger.info("当前用户是：{}， 角色是: {}", userDetails.getUsername(), authority.getAuthority());
            }

        } else {
            logger.warn("other ...........");
        }

        Optional<User> user = userRepository.findById(id);
        return user.orElse(null);
    }
}
