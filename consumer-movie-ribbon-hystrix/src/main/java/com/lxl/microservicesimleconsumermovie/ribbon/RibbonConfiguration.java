package com.lxl.microservicesimleconsumermovie.ribbon;
import org.springframework.context.annotation.Configuration;

//
// import com.netflix.loadbalancer.IRule;
// import com.netflix.loadbalancer.RandomRule;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
//
// /**
//  * @desc: ribbon配置
//  * @date & @author: 2018/11/20 16:01 & lxl
//  */
@Configuration
public class RibbonConfiguration {
//
//     /**
//      * 更改ribbon负载均衡规则为随机规则
//      *
//      * @return IRule
//      */
//     @Bean
//     public IRule ribbonRUle() {
//         return new RandomRule();
//     }
}
