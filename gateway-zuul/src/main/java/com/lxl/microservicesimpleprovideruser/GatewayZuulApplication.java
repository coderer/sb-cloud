package com.lxl.microservicesimpleprovideruser;

import com.lxl.microservicesimpleprovideruser.filter.PreRequestLogFilter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.filters.discovery.PatternServiceRouteMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

/**
 * @desc: 启动类
 * @author: lxl
 * @date: 2018/11/9 16:08
 * @version: v1.0
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class GatewayZuulApplication {

    /**
     * 使用正则表达式指定zuul的路由匹配规则
     *
     * @return PatternServiceRouteMapper
     */
    @Bean
    public PatternServiceRouteMapper serviceRouteMapper() {
        // 参数1: 微服务的正则  参数2: 服务对应的路由规则
        return new PatternServiceRouteMapper("(?<name>^.+)-(?<version>v.+$)", "%{version}/${name}");
    }

    @Bean
    public PreRequestLogFilter requestLogFilter() {
        return new PreRequestLogFilter();
    }

    @Bean
    @LoadBalanced
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(GatewayZuulApplication.class, args);
    }

}
