package com.lxl.microservicesimleconsumermovie.controller;

import com.lxl.microservicesimleconsumermovie.entity.User;
import com.lxl.microservicesimleconsumermovie.feign.UserFeignClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;


/**
 * @desc: 用户controller
 * @author: lxl
 * @date: 2018/11/9 16:01
 * @version: v1.0
 */
@RestController
public class MovieController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private UserFeignClient userFeignClient;

    /**
     * 根据id查找用户(使用restTemplate请求)
     * @param id 用户id
     * @return User
     */
    @GetMapping("/user/{id}")
    public User findById(@PathVariable Long id) {
        return this.restTemplate.getForObject("http://localhost:8000/" + id, User.class);
    }

    /**
     * 根据id查找用户(使用feign请求)
     * @param id 用户id
     * @return User
     */
    @GetMapping("/{id}")
    public User findUserById(@PathVariable Long id) {
        return this.userFeignClient.findById(id);
    }


    @GetMapping("/{id}/{name}")
    public User get1(@PathVariable("id") Long id, @PathVariable("name") String name) {
        return this.userFeignClient.getUserByIdAndNamePathVariableGet(id, name);
    }

    @PostMapping("/{id}/{name}")
    public User get2(@PathVariable("id") Long id, @PathVariable("name") String name) {
        return this.userFeignClient.getUserByIdAndNamePathVariablePost(id, name);
    }



    @GetMapping("/user")
    public User getUserByIdAndNameGet(@RequestParam("id") Long id, @RequestParam("name") String name) {
        return this.userFeignClient.getUserByIdAndNameRequestParamGet(id, name);
    }

    @GetMapping("/user/map")
    public User getUserByIdAndNamePost(@RequestParam("id") Long id, @RequestParam("name") String name) {
        HashMap<String, Object> map = new HashMap<String, Object>(2){
            {
                put("id", id);
                put("name", name);
            }
        };
        return this.userFeignClient.getUserByIdAndNameRequestParamMap(map);
    }


    @GetMapping("user/post")
    public User getUserPost() {
        User user = new User();
        user.setId(1L);
        user.setName("张三");
        return this.userFeignClient.getUserPostJson(user);
    }

}
