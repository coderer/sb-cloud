package com.lxl.microservicesimleconsumermovie.config;

import org.springframework.context.annotation.Configuration;

/**
 * @desc: feign配置
 * @date & @author: 2018/11/22 14:58 & lxl
 */
@Configuration
public class FeignConfiguration {

    // 配置注解规约(feign或springmvc)
    // @Bean
    // public Contract feignContract() {
    //     return new SpringMvcContract();
    // }
}
