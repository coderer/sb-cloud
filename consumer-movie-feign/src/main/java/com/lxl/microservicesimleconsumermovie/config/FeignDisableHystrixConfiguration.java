package com.lxl.microservicesimleconsumermovie.config;

import feign.Feign;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

/**
 * @desc: 禁用feign的hystrix配置
 * @date & @author: 2018/11/26 15:56 & lxl
 */
@Configuration
public class FeignDisableHystrixConfiguration {

    // @Bean
    // @Scope("prototype")
    // public Feign.Builder feignBuilder() {
    //     return Feign.builder();
    // }
}
