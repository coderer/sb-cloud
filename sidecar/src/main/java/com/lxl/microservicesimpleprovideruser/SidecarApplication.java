package com.lxl.microservicesimpleprovideruser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.cloud.netflix.sidecar.EnableSidecar;

/**
 * @desc: 启动类
 * @author: lxl
 * @date: 2018/11/9 16:08
 * @version: v1.0
 */
@EnableSidecar
@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class SidecarApplication {

    public static void main(String[] args) {
        SpringApplication.run(SidecarApplication.class, args);
    }

}
